const task3 = require('.');

describe('Task3 Test Suite', () => {
  const task8 = [
    [[2, 5, 1, 2, 3, 5, 1, 2, 4], 2],
    [[2, 1, 1, 2, 3, 5, 1, 2, 4], 1],
    [[2, 3, 4, 5], undefined],
    [[2, 5, 5, 2, 3, 5, 1, 2, 4], 5],
  ];
  const testCase = (test) =>
    it(`should return ${test[1]} when [${test[0]}]`, () => {
      expect(task3(test[0])).toBe(test[1]);
    });

  task8.map(testCase);
});

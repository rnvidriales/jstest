const firstRecurringChar = (arr = []) => {
  const elemCount = {
    elems: {},
    max: [undefined, 0],
  };
  /**
   * @returns true if all elements encountered have the same recurring number
   */
  const allSameRecurring = () =>
    Object.keys(elemCount.elems).every(
      (elem) => elemCount.elems[elem] === elemCount.max[1],
    );

  let count = 0;

  while (count < arr.length - elemCount.max[1]) {
    elemCount.elems[arr[count]] = elemCount.elems[arr[count]]
      ? elemCount.elems[arr[count]] + 1
      : 1;
    if (elemCount.max[1] < elemCount.elems[arr[count]]) {
      elemCount.max = [arr[count], elemCount.elems[arr[count]]];
    }
    count++;
  }

  return allSameRecurring() ? undefined : elemCount.max[0];
};

module.exports = firstRecurringChar;

const getBlockPolicyState = (domains) => {
  const policyArr = [];
  const numDomains = Object.keys(domains).length; 
  for (let i = 0; i < numDomains; i++) { // O(n)
    policyArr.push(Object.entries(domains)[i][1].policy); //O(n) * O(n)
  } 
  oneDomain = policyArr.some((item) => item === 'block'); // O(n)
  allDomains = policyArr.every((item) => item === 'block'); // O(n)
  return { oneDomain, allDomains };
};
// Total complexity O(2n + n²) => O(n²)

// More efficient version
const getBlockPolicyStateV2 = (domains) => {
  const isBlock = (item) => item === 'block';
  return Object.values(domains).reduce( //O(n)
    (acc, cur) => ({
      oneDomain: acc['oneDomain'] || isBlock(cur.policy),
      allDomains: acc['allDomains'] && isBlock(cur.policy),
    }),
    {
      oneDomain: false,
      allDomains: true,
    },
  );
};
// Total complexity O(n)

module.exports = {
  getBlockPolicyState,
  getBlockPolicyStateV2,
};

const performance = require('perf_hooks').performance;
const task2 = require('.');
const faker = require('faker');

const testExample = Array(1000)
  .fill(null)
  .reduce((acc, _, idx) => {
    acc[`${idx}.com`] = {
      policy: faker.datatype.boolean() ? 'block' : 'none',
    };
    return acc;
  }, {});

describe('Task1 Test Suite', () => {
  it('should return the same output', () => {
    expect(task2.getBlockPolicyState(testExample)).toEqual(
      task2.getBlockPolicyStateV2(testExample),
    );
    const allNone = {
      'one.com': { policy: 'none' },
    };
    expect(task2.getBlockPolicyState(allNone)).toEqual(
      task2.getBlockPolicyStateV2(allNone),
    );
    const allBlock = {
      'one.com': { policy: 'block' },
    };
    expect(task2.getBlockPolicyState(allBlock)).toEqual(
      task2.getBlockPolicyStateV2(allBlock),
    );
  });
  it('should be faster', () => {
    let t0 = performance.now();
    task2.getBlockPolicyState(testExample);
    let t1 = performance.now();
    const originalPerf = t1 - t0;
    t0 = performance.now();
    task2.getBlockPolicyStateV2(testExample);
    t1 = performance.now();
    const newPerf = t1 - t0;
    console.log(`
Performance
-------------------

Old version: ${originalPerf}
New version: ${newPerf}
`);
    expect(newPerf).toBeLessThan(originalPerf);
  });
});

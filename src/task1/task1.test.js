const task1 = require('.');

describe('Task1 Test Suite', () => {
  it('should return the expected string', () => {
    expect(
      task1([
        'apple',
        'pear',
        'lemon',
        'orange',
        'pineapple',
        'tomato',
        'lettuce',
        'mango',
        'apple',
        'pineapple',
        'lemon',
        'pear',
        'pear',
      ]),
    ).toBe(
      `Apple: 2,
Pear: 3,
Lemon: 2,
Orange: 1,
Pineapple: 2,
Tomato: 1,
Mango: 1,
Banana: 0`,
    );
  });
});

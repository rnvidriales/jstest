const expectedFruits = [
  'apple',
  'pear',
  'lemon',
  'orange',
  'pineapple',
  'tomato',
  'mango',
  'banana',
];

const capitalize = (str = '') => str.slice(0, 1).toUpperCase() + str.slice(1);

const formatFruits = (obj = {}) =>
  expectedFruits
    .map(
      (fruit) => `${capitalize(fruit)}: ${Number(obj[fruit] || 0).toFixed(0)}`,
    )
    .join(',\n');

const listOfFruits = (fruitList = []) =>
  fruitList.reduce((acc, currentFruit, idx, rest) => {
    const isLast = idx === rest.length - 1;
    acc[currentFruit] = acc[currentFruit] ? acc[currentFruit] + 1 : 1;
    if (isLast) {
      return formatFruits(acc);
    }
    return acc;
  }, {});

module.exports = listOfFruits;
